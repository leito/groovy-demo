/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy

def unaLista  = [1776, -1, 33, 99, 0, 928734928763]

println unaLista[0]
println unaLista.size();

//imprimir todos los elementos de la lista
unaLista.each({println it})

//podemos definir una lista sin elementos
def unaListaVacia = []