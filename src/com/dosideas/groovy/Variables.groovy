/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy
//definimos una variable. Se inicializan en null
def y
println y

//no es necesario usar el keyword "def"
x = 1
println x

//la misma variable puede tener distintos tipos de datos
x = false
println x

x = -3.1499392
println x

//podemos asignarle un objeto Java, en este caso una instancia de Date
x = new java.util.Date()
println x

//un String
x = "Hi"
println x

//podemos referenciar a variables dentro de un String, con el signo $
println "hola, $x"
