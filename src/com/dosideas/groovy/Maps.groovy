/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy

def unMap = ["Pedro" : "no terminó", "Juan": 99, "Zim":"no terminó"]

//acceder a un elemento con distinta sintáxis
println unMap["Pedro"]
println unMap.Pedro

//asignar un valor
unMap["Luis"] = "Otro valor"
println unMap["Luis"]

//podemos definir un map sin elementos
def unMapVacio = [:]
