/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy

nombres = ["Zim","Dib","Gaz","Tak"]

//recorriendo en forma "clásica"
for (i=0; i < nombres.size(); i++) {
    println nombres[i]
}

//o incrementando el índice de otra forma
for (i in 0..nombres.size()-1) {
    println nombres[i]
}


//o directamente sabiendo la cantidad de elementos
for (i in 0..3) {
    println nombres[i]
}

//una forma más simple y prolija, sin indice
for (i in nombres) {
    println i
}

//o un bucle que se repite varias veces
4.times { println nombres[it] }

//igual al anterior, pero sobre el tamaño de la colección
nombres.size().times { println nombres[it]}
