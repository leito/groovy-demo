/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dosideas.groovy.xml

//parseamos el archivo
def personas = new XmlSlurper().parse(new File("src/com/dosideas/groovy/xml/personas.xml"))

//imprimir todos los nombres
personas.persona.nombre.each( { println it })

//también lo podemos hacer genérico, sin concoer el nombre de los tags
println "el tag padre es: ${personas.name()}"
personas.children().each( { it.children().each( { println it }) } )

//Podemos acceder a un nodo en particular por orden
println personas.persona[2].nombre

//o filtrar por alguna condicion
grupoRestringido = personas.persona.findAll( { it.id.toInteger() > 10 })
grupoRestringido.each( { println it.nombre} )

//se puede acceder a los atributos de un nodo
edad = personas.persona[0].'@edad'

//o imprimir todas las edades
personas.persona.'@edad'.each( {println it } )

//imprimir todas las edades junto al nombre
personas.persona.each( { println "La edad de ${it.nombre} es ${it.'@edad'}" })
